# How to make a To Do App with Expo

Before diving into the code let's take a look at something, if you want to test the application you want to see if it has already installed.

- XCODE 
- Android Studio Code
	- Install Set up an expo environment with yarn or npm https://reactnative.dev/docs/environment-setup 
	- Install XCode if you haven’t and android studio (https://developer.android.com/studio) 
	- For android studio follow https://docs.expo.io/workflow/android-studio-emulator/

Well, lets begging 

**1 -**  Install expo global

`$ yarn global add expo-cli`

## Create the project 

**2 -**  Create a project named todo. Select the “blank (TypeScript)” template when prompted 

`$ expo init todo`

After install you’ll receive something like this: 

```
To run your project, navigate to the directory and run one of the following yarn commands.

- cd todo
- yarn start 

# Tip: you can open iOS, Android, or web from here, or run them directly with the commands below.
- yarn android
- yarn ios
- yarn web
```

**3 -**  Let's begin with our project 

```
$ cd todo 
$ yarn start
```

**4 -**  On Expo web, we can choose **Run on IOS Simulator** or **Run on Android Emulator/Device**. The device will run with our App.

## Cleaning App file and set the basis

**5 -** Open App.tsx, and we’ll make some changes. Remove everything below export default function App() . We will write the basic todo app code like:

```
<View style={styles.container}>
  {/* Header */}
  <View style={styles.content}>
    {/* Form */}
    <View  style={styles.list}>
      {/* List */}
    </View>
  </View>
</View>
```

## Adding the list (part 1)

**6 -** Add a **useState** to set our todos. We should `import useState like import React, { useState } from 'react';`. Then we add inside **App()** the next const:

```
const [todos, setTodos] = useState ([
    {text: 'buy criollitos', key: '1'},
    {text: 'create an app', key: '2'},
    {text: 'drink mate', key: '3'}
]);
```
 
**7 -** Add **FlatList** with `import { StyleSheet, View, Text, FlatList } from 'react-native';` to render that previous list in our code.

**8 -** Replace on **App.tsx** our comment `{/* List */}` with our **FlatList**. FlatList will receive a data array and we should specify how it will render that data information on renderItem, it could be a simple ReactNode or a complex component. First we will set a Text node with the text data like this:

```
<FlatList
    data={todos}
    renderItem={({item}) => (
      <Text>{item.text}<Text/>
    )}
/>
```

**9 -** Add some styles on **App.tsx**:
 
```
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    padding: 40,
  },
  list: {
    marginTop:20,
  },
});
```

## Adding a Header 
**10 -** Now we will create the **Header**. First create a `components` folder, and inside it, we will add a `Header.tsx`. Our Header will be  a very simple component with the title.
 
```
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Header() {
  return (
    <View style={styles.header}>
      <Text style={styles.title}>My Todos!</Text>
    </View>
  )
}
```
 
**11 -** Add the Header component to App.tsx. We should `import like this: import Header from './components/header';` and then replace the `{/* Header */}` with `<Header/>`

**12 -** Add some styles on **Header.tsx** like this: 
 
```
const styles = StyleSheet.create({
  header: {
    height: 120,
    paddingTop: 30,
    backgroundColor: 'coral',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
```

## Adding a list (part 2)
**13 -** Now we will create a component to render each item on the todo list. First we will create `TodoItem.tsx` on the `components` folder. The basic structure will be: 

```
import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export interface TodoItem {
  text: string;
  key: string;
}

export interface Props {
  todo: TodoItem;
}
export default function TodoItem (Props){
  return (
      <Text>{todo.text}</Text>
  )
}
```

As you can see, we write it in a very similar way as we set the `renderItem` in the App on **step 8**. We will receive the todo item and we will render it. 

**14 -** We will import this component on App with `import TodoItem from './components/todoItem';` and replace all the renderItem content like this: 
```
renderItem={({item}) => (
     <TodoItem todo={item}/>
   )}
```
 
**15 -** It must look the same as before. Now we will add a `onPress` action when we press an item. That will erase that item from the todo list. To do that, we will update **TodoItem Props** like this: 

```
export interface Props {
  todo: TodoItem;
  onPress: (key: string) => void;
}
```

And add a **TouchableOpacity** around our Text like this: 

   ```
<TouchableOpacity onPress={() => onPress(todo.key)}>
      <Text style={styles.text}>{todo.text}</Text>
    </TouchableOpacity>
```

Our **onPress** function will receive the item key and will remove it from the list. To do that, we must create that functionality on the **App.tsx**. We will create a **pressHandler** like this:

```
const pressHandler = (key: string) => {
    setTodos((prevTodos) => {
      return prevTodos.filter(todo => todo.key != key);
    })
  }
```

And then update our call to **TodoItem** on **FlatList renderItem** like this:

   ```
renderItem={({item}) => (
     <TodoItem todo={item} onPress={pressHandler}/>
   )}
```

And if we press on a single item, it will disappear from the list. 

**16 -** Now we can add it some styles like this ones:

```
const styles = StyleSheet.create({
  text: {
    padding: 16,
    marginTop: 16,
    borderColor: '#bbb',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 10
  },
});
```
## Add new Items
**17 -** Now we have the list, we can remove elements… we need to add new ones… 

We will create a new file, **AddTodo.tsx**, in the components folder.

On this component we need a **TextInput** (similar to the classic web input) to enter the text for the new todo item, and a submit button (another **TouchableOpacity**). The basic code will look like this:

```
import React, { useState } from 'react';
import {StyleSheet, Text, View, TextInput, Button, TouchableOpacity} from 'react-native';

export default function AddTodo(){
  return (
    <View>
      <TextInput 
        style={styles.input}
        placeholder= 'new todo...'
      />

      <TouchableOpacity style={styles.button} onPress={() => onSubmit(text)}>
        <Text style={styles.buttonText}>Add Todo</Text>
      </TouchableOpacity>
    </View>
  )
}
```

**18 -** Add some styles like this: 

```
const styles = StyleSheet.create({
    input: {
      marginBottom: 10,
      paddingHorizontal: 8,
      paddingVertical: 6,
      borderBottomWidth: 1,
      borderBottomColor: '#ddd'
    },
    button: {
      backgroundColor: 'coral',
      padding: 10,
      justifyContent: 'center',
      borderRadius: 10
    }, 
    buttonText: {
      color: 'white',
      textAlign: 'center',
      fontSize: 18,
      fontWeight: 'bold'
    }
  });
```
 
**19 -** Add **AddTodo** on **App.tsx** with `import AddTodo from './components/AddTodo;` and replace Form comment with `<AddTodo />`. That will show the input but will do nothing.

**20 -** Now, on **AddTodo.tsx**, we will create a **changeHandler** to handle the value on **TextInput**. To do that, we will create a **useState** like `const [text, setText] = useState('');` and then create the handler like this:

```
const changeHandler = (value: string) => {
  setText(value);
}
```

Then add `onChangeText={changeHandler}` on the **TextInput** tag like this:
```
<TextInput 
	style={styles.input}
	placeholder= 'new todo...'
	onChangeText={changeHandler}
/>
```

**21 -** And now, the **submit**. Similar to the remove item action, we need to add a handler to add a new one to the list. So, to do that, we need to update the **AddTodo Props** like this: 

```
export interface Props {
  onSubmit: (text: string) => void;
}

export default function AddTodo({onSubmit}: Props){
```
 
So we can know what to do when the user submits a new todo item. 
 
We create the **submitHandler** on **App.tsx** like this: 

 ```
const submitHandler = (text: string) => {
    setTodos((prevTodos) => {
      return [
        {text: text, key: Math.random().toString() },
        ...prevTodos]
    })
  }
```
 
Finally, on **App.tsx** we will update the `<AddTodo />` to:

```
<AddTodo onSubmit={submitHandler}/>
```
 
**21 -** Enjoy your first **React Native app**. We can improve this app as much as we want, like create a removedTodoList below the todo list like Google Keep, or maybe clean the TextInput when we submit... But that will be your own discovery trip because this is the end of this first workshop. Thanks for joining!

## Files

By the end of this workshop you have created a folder in root named components and inside you have created this files: 
- Header.tsx
- AddTodo.tsx
- TodoList.tsx

### Header component
```
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
 
export default function Header(){
 return (
   <View style={styles.header}>
     <Text style={styles.title}>My Todos!</Text>
   </View>
 )
}
 
const styles = StyleSheet.create({
 header: {
   height: 120,
   paddingTop: 30,
   backgroundColor: 'coral',
   justifyContent: 'center',
 },
 title: {
   textAlign: 'center',
   color: 'white',
   fontSize: 20,
   fontWeight: 'bold',
 },
});
```

### AddTodo Component
```
import React, { useState } from 'react';
import {StyleSheet, Text, View, TextInput, Button, TouchableOpacity} from 'react-native';
 
export interface Props {
 onSubmit: (text: string) => void;
}
 
export default function AddTodo({onSubmit}: Props){
 const [text, setText] = useState('');
 
 const changeHandler = (value: string) => {
     setText(value);
 }
 return (
   <View>
     <TextInput
       style={styles.input}
       placeholder= 'new todo...'
       onChangeText={changeHandler}
     />
 
     <TouchableOpacity style={styles.button} onPress={() => onSubmit(text)}>
       <Text style={styles.buttonText}>Add Todo</Text>
     </TouchableOpacity>
   </View>
 )
}
 
const styles = StyleSheet.create({
 input: {
   marginBottom: 10,
   paddingHorizontal: 8,
   paddingVertical: 6,
   borderBottomWidth: 1,
   borderBottomColor: '#ddd'
 },
 button: {
   backgroundColor: 'coral',
   padding: 10,
   justifyContent: 'center',
   borderRadius: 10
 },
 buttonText: {
   color: 'white',
   textAlign: 'center',
   fontSize: 18,
   fontWeight: 'bold'
 }
});
```
 
### TodoList Component
```
import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
 
export interface TodoItem {
 text: string;
 key: string;
}
 
export interface Props {
 todo: TodoItem;
 onPress: (key: string) => void;
}
 
export default function Header({todo, onPress}: Props){
 return (
   <TouchableOpacity onPress={() => onPress(todo.key)}>
     <Text style={styles.text}>{todo.text}</Text>
   </TouchableOpacity>
 )
}
 
const styles = StyleSheet.create({
 text: {
   padding: 16,
   marginTop: 16,
   borderColor: '#bbb',
   borderWidth: 1,
   borderStyle: 'dashed',
   borderRadius: 10
 },
});
```
 
#### TodoList: a better version 
We can install this package:

`yarn add react-native-elements`

And update the code like this:

```
import React from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { Icon } from 'react-native-elements'
export interface ITodo {
todo: { text: string; key: string }
handleDelete: (id: string) => void
}
export interface ITodoList {
todos: { text: string; key: string }[]
handleDelete: (key: string) => void
}
const Todo = ({ todo: { text, key }, handleDelete }: ITodo) => {
return (
  <View style={styles.todo}>
    <Text>{text}</Text>
    <Icon
      name='close-circle-outline'
      type='ionicon'
      color='coral'
      style={styles.icon}
      size={25}
      onPress={() => {
        handleDelete(key)
      }}
    />
  </View>
)
}
export default function TodoList({ todos, handleDelete }: ITodoList) {
return (
  <View style={styles.list}>
    <FlatList
      data={todos}
      renderItem={({ item }) => (
        <Todo todo={item} handleDelete={handleDelete} />
      )}
    />
  </View>
)
}
const styles = StyleSheet.create({
list: {
  width: '100%',
  maxWidth: 400,
  borderTopColor: '#ccc',
  borderTopWidth: 1
},
todo: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingTop: 10,
  paddingLeft: 20,
  paddingBottom: 10,
  paddingRight: 20,
  borderBottomColor: '#ccc',
  borderBottomWidth: 1
},
icon: {
  marginLeft: 5
}
})
```

### App File
```
import React, { useState } from 'react';
import { StyleSheet, View, FlatList} from 'react-native';
import Header from './components/header';
import TodoItem from './components/todoItem';
import AddTodo from './components/addTodo';
 
export default function App() {
 const [todos, setTodos] = useState ([
   {text: 'buy criollitos', key: '1'},
   {text: 'create an app', key: '2'},
   {text: 'drink mate', key: '3'}
 ]);
 
 const pressHandler = (key: string) => {
   console.log(key);
   setTodos((prevTodos) => {
     return prevTodos.filter(todo => todo.key != key);
   })
 }
 
 const submitHandler = (text: string) => {
   setTodos((prevTodos) => {
     return [
       {text: text, key: Math.random().toString() },
       ...prevTodos]
   })
 }
 
 return (
   <View style={styles.container}>
     <Header />
     <View style={styles.content}>
       <AddTodo onSubmit={submitHandler}/>
       <View style={styles.list}>
         <FlatList
           data={todos}
           renderItem={({item}) => (
             <TodoItem todo={item} onPress={pressHandler}/>
           )}
         />
       </View>
     </View>
   </View>
 );
}
 
const styles = StyleSheet.create({
 container: {
   flex: 1,
   backgroundColor: 'white',
 },
 content: {
   padding: 40,
 },
 list: {
   marginTop:20,
 },
});
```
 


